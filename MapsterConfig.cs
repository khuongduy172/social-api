﻿using Mapster;
using social_network.src.Dtos;
using social_network.src.Models;

namespace social_network
{
    public static class MapsterConfig
    {
        public static void RegisterMapsterConfiguration(this IServiceCollection services)
        {
            //TypeAdapterConfig<StatusImage, RandomImagesDto>
            //    .NewConfig()
            //    .Map(dest => dest.FullName, src => $"{src.Title} {src.FirstName} {src.LastName}");

            TypeAdapterConfig<User, FollowNotiDto>
                .NewConfig()
                .Map(dest => dest.FollowerId, src => src.Id);

            TypeAdapterConfig<Status, StatusDto>
                .NewConfig()
                .Map(dest => dest.ReactCount, src => src.Reacts.Count)
                .Map(dest => dest.CommentCount, src => src.Comments.Count);

            TypeAdapterConfig<User, UserDto>
                .NewConfig()
                .Map(dest => dest.PostCount, src => src.Statuses.Count)
                .Map(dest => dest.FollowerCount, src => src.Followers.Count)
                .Map(dest => dest.FollowingCount, src => src.Followings.Count);

        }
    }
}