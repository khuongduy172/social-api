# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:6.0-nanoserver-ltsc2022 AS build-env
WORKDIR /app

# Copy the project file(s) to the container
COPY *.csproj ./
RUN dotnet restore

# Copy the entire project to the container
COPY . .
RUN dotnet publish -c Release -o out

# Use a lightweight image for runtime
FROM mcr.microsoft.com/dotnet/aspnet:6.0-nanoserver-ltsc2022
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "social_network.dll"]