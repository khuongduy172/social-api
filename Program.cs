using social_network.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using social_network.src.Common.Interfaces;
using System.Text.Json.Serialization;
using social_network;
using social_network.src.Middlewares;
using social_network.src.Hubs;
using social_network.src.Common.Dapper;
using social_network.src.Common.Mailing;
using social_network.src.Services;
using social_network.src.Configurations;
using social_network.src.Seeders;

var builder = WebApplication.CreateBuilder(args);
{
    // Add services to the container.

    // Db
    builder.Services.AddDbContext<SocialDbContext>(options =>
    {
        var conn = builder.Configuration.GetConnectionString("DefaultConnection");
        options.UseMySql(conn, ServerVersion.AutoDetect(conn));
    });

    //string hangfireConnectionString = builder.Configuration.GetConnectionString("HangfireConnection");
    //builder.Services.AddHangfireConfiguration(hangfireConnectionString);

    // Mapster
    builder.Services.RegisterMapsterConfiguration();

    builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    // SignalR
    builder.Services.AddSignalR()
        .AddJsonProtocol(option =>
        {
            option.PayloadSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        });

    // Register dependency injection
    builder.Services.RegisterServices(builder.Configuration);
    builder.Services.RegisterSeeder();

    // register currentUser DI
    builder.Services.AddCurrentUser();
    builder.Services.AddException();

    builder.Services.AddAuthentication(options =>
    {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
    }).AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidIssuer = builder.Configuration["Jwt:Issuer"],
            //ValidAudience = builder.Configuration["Jwt:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
            ValidateIssuer = true,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true
        };
    });

    builder.Services.AddSingleton<DapperContext>();

    builder.Services.AddControllers().AddJsonOptions(x =>
                    x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

    // Swagger
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwagger();

    builder.Services.Configure<MailSettings>(builder.Configuration.GetSection(nameof(MailSettings)));
    builder.Services.Configure<AWSCredentials>(builder.Configuration.GetSection(nameof(AWSCredentials)));
}

var app = builder.Build();

await app.RunSeederBeforeMigrationsAsync();

// auto migrate db on startup
app.MigrateDatabase();

await app.RunSeederAsync();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//}
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
});

app.UseHttpsRedirection();

//app.UseHangfireDashboard();

//BackgroundJobCreator.Create();

app.UseAuthentication();
app.UseAuthorization();

// use middleware
app.UseExceptionMiddleware();
app.UseCurrentUserMiddleware();

// app.UseCors();

app.MapControllers();
app.MapHub<CoreHub>("/corehub");

app.Run();