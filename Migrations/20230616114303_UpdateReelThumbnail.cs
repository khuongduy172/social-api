﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace social_network.Migrations
{
    public partial class UpdateReelThumbnail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ThumbnailKey",
                table: "Reel",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "ThumbnailUrl",
                table: "Reel",
                type: "longtext",
                nullable: false)
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThumbnailKey",
                table: "Reel");

            migrationBuilder.DropColumn(
                name: "ThumbnailUrl",
                table: "Reel");
        }
    }
}
