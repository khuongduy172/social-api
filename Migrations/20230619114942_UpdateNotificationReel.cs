﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace social_network.Migrations
{
    public partial class UpdateNotificationReel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ReelId",
                table: "Notification",
                type: "char(36)",
                nullable: true,
                collation: "ascii_general_ci");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReelId",
                table: "Notification");
        }
    }
}
