﻿using AutoMapper;
using social_network.src.Dtos;
using social_network.src.Models;

namespace social_network
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Status, StatusDto>()
                .ForMember(dest => dest.ReactCount, opt => opt.MapFrom(src => src.Reacts.Count))
                .ForMember(dest => dest.IsReacted, opt => opt.Ignore())
                .ForMember(dest => dest.IsOwner, opt => opt.Ignore())
                .ForMember(dest => dest.CommentCount, opt => opt.MapFrom(src => src.Comments.Count));

            CreateMap<User, UserDto>()
                .ForMember(dest => dest.PostCount, opt => opt.MapFrom(src => src.Statuses == null ? 0 : src.Statuses.Count))
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Statuses == null ? null : src.Statuses.SelectMany(s => s.StatusImages).OrderByDescending(s => s.CreatedAt).ToList()))
                .ForMember(dest => dest.Reels, opt => opt.MapFrom(src => src.Reels == null ? null : src.Reels.OrderByDescending(x => x.CreatedAt).ToList()))
                .ForMember(dest => dest.FollowerCount, opt => opt.MapFrom(src => src.Followers == null ? 0 : src.Followers.Count))
                .ForMember(dest => dest.FollowingCount, opt => opt.MapFrom(src => src.Followings == null ? 0 : src.Followings.Count));

            CreateMap<User, SearchUserDto>();

            CreateMap<StatusImage, StatusImageDto>();
            CreateMap<StatusImage, RandomImagesDto>();
            CreateMap<UserViewStory, UserViewStoryDto>();
            CreateMap<Follow, FollowDto>();
            CreateMap<Notification, NotiDto>()
                .ForMember(dest => dest.TypeNoti, opt => opt.MapFrom(src => src.TypeNoti.ToString()));
            CreateMap<Comment, CommentDto>();
            CreateMap<Message, MessageDto>();
            CreateMap<Story, StoryDto>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type.ToString()));
            CreateMap<Reel, ReelDto>()
                .ForMember(dest => dest.ReactCount, opt => opt.MapFrom(src => src.ReelReacts.Count))
                .ForMember(dest => dest.IsReacted, opt => opt.Ignore())
                .ForMember(dest => dest.IsOwner, opt => opt.Ignore())
                .ForMember(dest => dest.CommentCount, opt => opt.MapFrom(src => src.ReelComments.Count));
        }
    }
}