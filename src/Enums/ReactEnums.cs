namespace social_network.Enums
{
    public enum ReactEnums
    {
        Like,
        Dislike,
        Love,
        Haha,
        Wow,
        Sad,
        Angry
    }
}