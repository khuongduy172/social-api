﻿namespace social_network.src.Enums
{
    public enum FollowStatusEnums
    {
        None,
        FollowedBy,
        Following,
    }
}