﻿namespace social_network.src.Enums;

public enum TypeNotiEnum
{
    Follow,
    React,
    Comment,
    ReelReact,
    ReelComment,
}