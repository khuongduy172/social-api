﻿using RazorEngineCore;
using social_network.src.Common.Interfaces;
using System.Text;

namespace social_network.src.Common.Mailing
{
    public interface IEmailTemplateService : ITransientService
    {
        string GenerateEmailTemplate<T>(string templateName, T mailTemplateModel);
    }

    public class EmailTemplateService : IEmailTemplateService
    {
        public string GenerateEmailTemplate<T>(string templateName, T mailTemplateModel)
        {
            string template = GetTemplate(templateName);

            IRazorEngine razorEngine = new RazorEngine();
            IRazorEngineCompiledTemplate modifiedTemplate = razorEngine.Compile(template);

            return modifiedTemplate.Run(mailTemplateModel);
        }

        public string GetTemplate(string templateName)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string tmplFolder = Path.Combine(baseDirectory, "Common", "Mailing", "MailTemplates");
            string filePath = Path.Combine(tmplFolder, $"{templateName}.cshtml");

            using var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using var sr = new StreamReader(fs, Encoding.Default);
            string mailText = sr.ReadToEnd();
            sr.Close();

            return mailText;
        }
    }
}