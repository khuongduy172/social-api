﻿namespace social_network.src.Common.Mailing.MailModels;

public class PasswordResetMailModel
{
    public string Email { get; set; } = default!;
    public string Password { get; set; } = default!;
}