﻿using System.Net;

namespace social_network.src.Common
{
    public class BadRequestException : CustomException
    {
        public BadRequestException(string message)
            : base(message, null, HttpStatusCode.BadRequest)
        {
        }
    }
}
