﻿using System.Text.Json.Serialization;

namespace social_network.src.Dtos;

public class GetStoryResponse
{
    [JsonPropertyName("user_id")]
    public Guid UserId { get; set; }

    [JsonPropertyName("user_image")]
    public string UserImage { get; set; } = default!;

    [JsonPropertyName("user_name")]
    public string UserName { get; set; } = default!;

    [JsonPropertyName("stories")]
    public List<StoryDto> Stories { get; set; } = new List<StoryDto>();
}