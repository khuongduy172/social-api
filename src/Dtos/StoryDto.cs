﻿using System.Text.Json.Serialization;

namespace social_network.src.Dtos;

public class StoryDto
{
    [JsonPropertyName("story_id")]
    public Guid Id { get; set; }

    [JsonPropertyName("story_image")]
    public string Url { get; set; } = default!;

    [JsonPropertyName("type")]
    public string Type { get; set; } = default!;

    [JsonIgnore]
    public Guid OwnerId { get; set; }

    [JsonIgnore]
    public UserDto Owner { get; set; } = default!;

    [JsonIgnore]
    public List<UserViewStoryDto> UserViewStory { get; set; } = default!;

    [JsonIgnore]
    public bool IsOwner { get; set; }

    [JsonIgnore]
    public bool Viewed { get; set; }

    public DateTime CreatedAt { get; set; }
}