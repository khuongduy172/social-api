namespace social_network.Dtos;

public class RegisterRequest
{
    public string Email { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string PasswordConfirm { get; set; }
    public string Name { get; set; }
    public bool IsMale { get; set; }

    public RegisterRequest(string email, string username, string password, string passwordConfirm, string name, bool isMale)
    {
        Email = email;
        Username = username;
        Password = password;
        PasswordConfirm = passwordConfirm;
        Name = name;
        IsMale = isMale;
    }
}