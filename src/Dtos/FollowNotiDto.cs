﻿namespace social_network.src.Dtos
{
    public class FollowNotiDto
    {
        public FollowNotiDto(Guid followerId, string name, string avatar)
        {
            FollowerId = followerId;
            Name = name;
            Avatar = avatar;
        }

        public Guid FollowerId { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}