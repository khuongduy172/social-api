﻿namespace social_network.src.Dtos
{
    public class CreateReelRequest
    {
        public string Caption { get; set; } = default!;
        public IFormFile Video { get; set; } = default!;
    }
}