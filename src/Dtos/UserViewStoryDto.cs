﻿namespace social_network.src.Dtos;

public class UserViewStoryDto
{
    public Guid UserId { get; set; }
    public Guid StoryId { get; set; }
}