﻿namespace social_network.src.Dtos
{
    public class CreateStatusRequest
    {
        public string? Content { get; set; }
        public List<IFormFile>? Files { get; set; }
    }
}
