﻿namespace social_network.src.Dtos
{
    public class CommentDto
    {
        public Guid Id { get; set; }
        public Guid StatusId { get; set; }
        public Guid OwnerId { get; set; }
        public string? Content { get; set; }
        public bool IsOwner { get; set; } = false;
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public UserDto Owner { get; set; } = default!;
    }
}