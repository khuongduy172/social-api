﻿namespace social_network.src.Dtos
{
    public class SearchUserDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public string? Avatar { get; set; }
        public bool IsFollowing { get; set; }
        public List<FollowDto> Followers { get; set; } = default!;
    }
}