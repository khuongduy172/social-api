﻿namespace social_network.src.Dtos
{
    public class UpdateAvatarRequest
    {
        public IFormFile File { get; set; } = default!;
    }
}