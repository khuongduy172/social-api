﻿namespace social_network.src.Dtos
{
    public class NotiDto
    {
        public Guid Id { get; set; }
        public string TypeNoti { get; set; } = default!;
        public Guid OwnerId { get; set; }
        public Guid? FromId { get; set; }
        public Guid? StatusId { get; set; }
        public UserDto FromUser { get; set; } = default!;
        public DateTime CreatedAt { get; set; }
        public bool IsRead { get; set; }
    }
}