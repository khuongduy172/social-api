﻿namespace social_network.src.Dtos
{
    public class StatusDto
    {
        public StatusDto()
        {
        }

        public Guid Id { get; set; }
        public string? Content { get; set; }
        public Guid OwnerId { get; set; }
        public bool IsReacted { get; set; } = false;
        public bool IsOwner { get; set; } = false;
        public int ReactCount { get; set; }
        public int CommentCount { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public UserDto Owner { get; set; } = default!;
        public List<StatusImageDto> StatusImages { get; set; } = default!;
    }
}