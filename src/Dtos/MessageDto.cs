﻿namespace social_network.src.Dtos;

public class MessageDto
{
    public Guid SenderId { get; set; }
    public Guid ReceiverId { get; set; }
    public string? Content { get; set; }
    public string? ImageUrl { get; set; }
    public bool IsRead { get; set; }
    public DateTime? ReadAt { get; set; }
    public bool IsDeleted { get; set; }

    public UserDto Sender { get; set; } = default!;
    public UserDto Receiver { get; set; } = default!;
}