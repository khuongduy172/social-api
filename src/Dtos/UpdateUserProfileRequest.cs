﻿namespace social_network.src.Dtos
{
    public class UpdateUserProfileRequest
    {
        public string? Bio { get; set; }
        public string? Name { get; set; }
        public string? Username { get; set; }
        public DateTime? DayOfBirth { get; set; }
        public string? Gender { get; set; }
        public string? Phone { get; set; }
    }
}