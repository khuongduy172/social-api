﻿using System.ComponentModel.DataAnnotations;

namespace social_network.src.Dtos
{
    public class CreateCommentRequest
    {
        [Required]
        public string Content { get; set; } = default!;
    }
}