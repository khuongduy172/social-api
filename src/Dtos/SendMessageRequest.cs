﻿namespace social_network.src.Dtos
{
    public class SendMessageRequest
    {
        public Guid ReceiverId { get; set; }
        public string? Content { get; set; }
        public IFormFile? Image { get; set; }
    }
}