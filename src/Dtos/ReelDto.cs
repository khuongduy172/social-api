﻿using social_network.src.Models;

namespace social_network.src.Dtos
{
    public class ReelDto
    {
        public Guid Id { get; set; }
        public string Url { get; set; } = default!;
        public string ThumbnailUrl { get; set; } = default!;
        public string? Caption { get; set; }
        public Guid OwnerId { get; set; }
        public virtual UserDto Owner { get; set; } = default!;
        public int ReactCount { get; set; }
        public int CommentCount { get; set; }
        public bool IsOwner { get; set; }
        public bool IsReacted { get; set; }
        public virtual ICollection<ReelReact> ReelReacts { get; set; } = default!;
    }
}