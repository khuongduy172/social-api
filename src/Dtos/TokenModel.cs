﻿namespace social_network.src.Dtos
{
    public class TokenModel
    {
        public string Token { get; set; } = string.Empty;
        public string RefreshToken { get; set; } = string.Empty;
    }
}