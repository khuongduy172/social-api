﻿using social_network.src.Enums;
using System.ComponentModel.DataAnnotations;

namespace social_network.src.Dtos;

public class CreateStoryRequest
{
    [Required]
    public IFormFile File { get; set; } = default!;
}