﻿namespace social_network.src.Dtos
{
    public class FollowRequest
    {
        public Guid UserId { get; set; }
    }
}
