﻿namespace social_network.src.Dtos
{
    public class FollowDto
    {
        public Guid UserId { get; set; }
        public Guid FollowerId { get; set; }
        public UserDto Follower { get; set; } = default!;
    }
}