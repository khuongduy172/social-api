﻿namespace social_network.src.Dtos
{
    public class StatusImageDto
    {
        public string Name { get; set; } = default!;
        public Guid StatusId { get; set; }
        public string Url { get; set; } = default!;
    }
}