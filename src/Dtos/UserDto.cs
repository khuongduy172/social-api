﻿using social_network.src.Enums;

namespace social_network.src.Dtos
{
    public class UserDto
    {
        public UserDto() { }
        public UserDto(Guid id, string email, string? bio, string name, string? avatar, DateTime dayOfBirth, string gender, string? phone)
        {
            Id = id;
            Email = email;
            Bio = bio;
            Name = name;
            Avatar = avatar;
            DayOfBirth = dayOfBirth;
            Gender = gender;
            Phone = phone;
        }

        public Guid Id { get; set; }
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Bio { get; set; }
        public string Name { get; set; }
        public string? Avatar { get; set; }
        public DateTime DayOfBirth { get; set; }
        public string Gender { get; set; }
        public string? Phone { get; set; }
        public int PostCount { get; set; }
        public int FollowerCount { get; set; }
        public int FollowingCount { get; set; }
        public bool IsOwner { get; set; } = false;
        public string FollowStatus { get; set; } = nameof(FollowStatusEnums.None);
        public List<FollowDto> Followers { get; set; } = default!;
        public List<FollowDto> Followings { get; set; } = default!;
        public List<StatusImageDto> Images { get; set; } = default!;
        public List<ReelDto> Reels { get; set; } = default!;
    }
}