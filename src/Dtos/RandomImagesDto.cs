﻿namespace social_network.src.Dtos
{
    public class RandomImagesDto
    {
        public RandomImagesDto()
        {
            Name = string.Empty;
            Url = string.Empty;
        }

        public RandomImagesDto(string name, Guid statusId, string url)
        {
            Name = name;
            StatusId = statusId;
            Url = url;
        }

        public string Name { get; set; }
        public Guid StatusId { get; set; }
        public string Url { get; set; }
    }
}
