﻿using System.ComponentModel.DataAnnotations;

namespace social_network.src.Dtos;

public class PasswordResetRequest
{
    [Required]
    public string Email { get; set; } = default!;
}