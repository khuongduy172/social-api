﻿namespace social_network.src.Dtos
{
    public class ImageObject
    {
        public string FileName { get; set; } = default!;
        public string Url { get; set; } = default!;
    }
}