using Firebase.Storage;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using System.Text.RegularExpressions;

namespace social_network.src.Services;

public interface IFileUpload : ITransientService
{
    Task<ImageObject> UploadToFirebaseAsync(IFormFile file);

    Task DeleteAsync(string fileName);
}

public class FileUpload : IFileUpload
{
    public async Task<string> Upload(IFormFile file, string fileName)
    {
        if (file.Length > 0)
        {
            var folderName = Path.Combine("Resources", "Images"); // in source code
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            CreateIfMissing(pathToSave);
            var fullPath = Path.Combine(pathToSave, fileName);
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }
            return fullPath;
        }
        return string.Empty;
    }

    public void Delete(string path)
    {
        File.Delete(path);
    }

    public async Task<ImageObject> UploadToFirebaseAsync(IFormFile file)
    {
        var fileName = Regex.Replace(DateTime.UtcNow.ToString() + file.FileName, @"[^0-9a-zA-Z\._]", "");
        var path = await Upload(file, fileName);
        var stream = File.Open(path, FileMode.Open);
        var task = new FirebaseStorage(
            "instagram-381915.appspot.com")
            .Child("instagram")
            .Child(fileName)
            .PutAsync(stream);
        string downloadUrl = await task;
        stream.Close();
        Delete(path);
        return new ImageObject()
        {
            FileName = fileName,
            Url = downloadUrl,
        };
    }

    public async Task DeleteAsync(string fileName)
    {
        try
        {
            var task = new FirebaseStorage(
                "instagram-381915.appspot.com")
                .Child("instagram")
                .Child(fileName)
                .DeleteAsync();
            await task;
        }
        catch { }
    }

    private void CreateIfMissing(string path)
    {
        bool folderExists = Directory.Exists(path);
        if (!folderExists)
            Directory.CreateDirectory(path);
    }
}