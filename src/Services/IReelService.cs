﻿using AutoMapper;
using Firebase.Auth;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.Enums;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using social_network.src.Enums;
using social_network.src.Hubs;
using social_network.src.Models;

namespace social_network.src.Services
{
    public interface IReelService : ITransientService
    {
        Task Create(Reel reel, CancellationToken cancellationToken);

        Task<List<ReelDto>> Get(Guid userId, CancellationToken cancellationToken);

        Task<List<ReelDto>> GetUser(Guid userId, Guid currentUserId, CancellationToken cancellationToken);

        Task View(Guid userId, Guid reelId, CancellationToken cancellationToken);

        Task Delete(Guid userId, Guid reelId, CancellationToken cancellationToken);

        Task ReactAsync(Guid reelId, CancellationToken cancellationToken);
    }

    public class ReelService : IReelService
    {
        private readonly SocialDbContext _context;
        private readonly IMapper _mapper;
        private readonly IStorageService _storageService;
        private readonly ICurrentUser _currentUser;
        private readonly IHubContext<CoreHub> _hubContext;

        public ReelService(SocialDbContext context, IMapper mapper, IStorageService storageService, ICurrentUser currentUser, IHubContext<CoreHub> hubContext)
        {
            _context = context;
            _mapper = mapper;
            _storageService = storageService;
            _currentUser = currentUser;
            _hubContext = hubContext;
        }

        public async Task Create(Reel reel, CancellationToken cancellationToken)
        {
            await _context.Reels.AddAsync(reel);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task Delete(Guid userId, Guid reelId, CancellationToken cancellationToken)
        {
            var reel = await _context.Reels.FirstOrDefaultAsync(x => x.Id == reelId, cancellationToken)
                ?? throw new NotFoundException("reel not found!");

            if (reel.OwnerId != userId)
            {
                throw new ForbiddenException("you are not owner of this reel!");
            }

            await _storageService.DeleteAsync(reel.Key);
            await _storageService.DeleteAsync(reel.ThumbnailKey);

            _context.Reels.Remove(reel);
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<List<ReelDto>> Get(Guid userId, CancellationToken cancellationToken)
        {
            var viewedReelIds = await _context.UserViewReels.Where(x => x.UserId == userId).Select(x => x.ReelId).ToListAsync(cancellationToken);
            var reels = await _context.Reels
                .Where(x => !viewedReelIds.Contains(x.Id))
                .Include(x => x.Owner)
                .Include(x => x.ReelReacts)
                .Include(x => x.ReelComments)
                .Take(10)
                .ToListAsync(cancellationToken);
            if (reels.Count == 0)
            {
                reels = await _context.Reels
                    .Where(x => viewedReelIds.Contains(x.Id))
                    .Include(x => x.Owner)
                    .Include(x => x.ReelReacts)
                    .Include(x => x.ReelComments)
                    .Take(10)
                    .ToListAsync(cancellationToken);
            }
            var reelDots = _mapper.Map<List<ReelDto>>(reels);
            foreach (var reel in reelDots)
            {
                reel.IsOwner = userId == reel.OwnerId;
                reel.IsReacted = reel.ReelReacts.Any(x => x.UserId == userId);
            }

            return reelDots;
        }

        public async Task<List<ReelDto>> GetUser(Guid userId, Guid currentUserId, CancellationToken cancellationToken)
        {
            var reels = await _context.Reels
                .Where(x => x.OwnerId == userId)
                .Include(x => x.Owner)
                .Include(x => x.ReelReacts)
                .Include(x => x.ReelComments)
                .OrderByDescending(x => x.CreatedAt)
                .ToListAsync(cancellationToken);
            var reelDots = _mapper.Map<List<ReelDto>>(reels);

            foreach (var reel in reelDots)
            {
                reel.IsOwner = currentUserId == reel.OwnerId;
                reel.IsReacted = reel.ReelReacts.Any(x => x.UserId == currentUserId);
            }

            return reelDots;
        }

        public async Task ReactAsync(Guid reelId, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            var reel = await _context.Reels.FirstOrDefaultAsync(s => s.Id == reelId) ?? throw new NotFoundException("Reel not found!");
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId) ?? throw new NotFoundException("User not found!");
            var react = await _context.ReelReacts.FirstOrDefaultAsync(r => r.UserId == userId && r.ReelId == reelId);
            if (react == null)
            {
                react = new ReelReact()
                {
                    ReelId = reelId,
                    UserId = userId,
                    Type = ReactEnums.Love,
                };

                _context.ReelReacts.Add(react);

                if (userId != reel.OwnerId)
                {
                    var noti = new Notification()
                    {
                        OwnerId = reel.OwnerId,
                        FromId = userId,
                        TypeNoti = TypeNotiEnum.ReelReact,
                        ReelId = reelId,
                    };
                    _context.Notifications.Add(noti);
                    await _context.SaveChangesAsync(cancellationToken);

                    await _hubContext.Clients.Groups(reel.OwnerId.ToString()).SendAsync("Notification", noti, cancellationToken);
                }
            }
            else
            {
                _context.Remove(react);
                var noti = await _context.Notifications.FirstOrDefaultAsync(x => x.OwnerId == reel.OwnerId && x.ReelId == reelId && x.FromId == userId, cancellationToken);
                if (noti != null)
                {
                    _context.Notifications.Remove(noti);
                }
            }
            await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task View(Guid userId, Guid reelId, CancellationToken cancellationToken)
        {
            var reel = await _context.Reels.FirstOrDefaultAsync(x => x.Id == reelId) ?? throw new NotFoundException("reel not found!");

            var userViewedReel = await _context.UserViewReels.FirstOrDefaultAsync(x => x.UserId == userId && x.ReelId == reelId, cancellationToken);

            if (userViewedReel == null)
            {
                var userViewReel = new UserViewReel()
                {
                    UserId = userId,
                    ReelId = reelId,
                };

                await _context.UserViewReels.AddAsync(userViewReel);
                await _context.SaveChangesAsync(cancellationToken);
            }
        }
    }
}