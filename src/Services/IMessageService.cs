﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using social_network.src.Hubs;
using social_network.src.Models;

namespace social_network.src.Services;

public interface IMessageService : ITransientService
{
    Task Create(SendMessageRequest request, CancellationToken cancellationToken);

    Task<PaginationResponse<MessageDto>> Get(Guid userId, PaginationFilter filter, CancellationToken cancellationToken);

    Task<List<MessageDto>> GetUserList(CancellationToken cancellationToken);
}

public class MessageService : IMessageService
{
    private readonly IStorageService _storageService;
    private readonly SocialDbContext _context;
    private readonly IHubContext<CoreHub> _hubContext;
    private readonly ICurrentUser _currentUser;
    private readonly IMapper _mapper;

    public MessageService(IStorageService storageService, SocialDbContext context, IHubContext<CoreHub> hubContext, ICurrentUser currentUser, IMapper mapper)
    {
        _storageService = storageService;
        _context = context;
        _hubContext = hubContext;
        _currentUser = currentUser;
        _mapper = mapper;
    }

    public async Task Create(SendMessageRequest request, CancellationToken cancellationToken)
    {
        var senderId = _currentUser.GetUserId();
        var image = new ImageObject();
        if (request.Image != null)
        {
            image = await _storageService.UploadAsync(request.Image);
        }

        var message = new Message(senderId, request.ReceiverId, request.Content, image.Url, image.FileName);

        await _context.Messages.AddAsync(message, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);

        var result = await _context.Messages
            .Include(x => x.Sender)
            .Include(x => x.Receiver)
            .FirstOrDefaultAsync(x => x.Id == message.Id, cancellationToken);

        var messageDto = _mapper.Map<MessageDto>(result);

        await _hubContext.Clients.Groups($"{senderId}-{request.ReceiverId}").SendAsync("RecieveMessage", messageDto, cancellationToken);
        await _hubContext.Clients.Groups($"{request.ReceiverId}-{senderId}").SendAsync("RecieveMessage", messageDto, cancellationToken);
    }

    public async Task<PaginationResponse<MessageDto>> Get(Guid userId, PaginationFilter filter, CancellationToken cancellationToken)
    {
        var currentUserId = _currentUser.GetUserId();
        var query = _context.Messages
            .Where(x => (x.SenderId == currentUserId && x.ReceiverId == userId) || (x.SenderId == userId && x.ReceiverId == currentUserId))
            .OrderByDescending(x => x.CreatedAt);

        int total = await query.CountAsync(cancellationToken);
        var messages = await query
            .Skip((filter.PageNumber - 1) * filter.PageSize)
            .Take(filter.PageSize)
            .ToListAsync(cancellationToken);

        var messageDtos = _mapper.Map<List<MessageDto>>(messages);
        return new PaginationResponse<MessageDto>(messageDtos, total, filter.PageNumber, filter.PageSize);
    }

    public async Task<List<MessageDto>> GetUserList(CancellationToken cancellationToken)
    {
        var currentUserId = _currentUser.GetUserId();

        var messages1 = await _context.Messages
            .Where(x => x.SenderId == currentUserId)
            .Include(x => x.Sender)
            .Include(x => x.Receiver)
            .GroupBy(x => x.ReceiverId)
            .Select(g => new
            {
                UserId = g.Key,
                LatestMessage = g.OrderByDescending(x => x.CreatedAt).First(),
            })
            .ToListAsync(cancellationToken);

        var messages2 = await _context.Messages
            .Where(x => x.ReceiverId == currentUserId)
            .Include(x => x.Sender)
            .Include(x => x.Receiver)
            .GroupBy(x => x.SenderId)
            .Select(g => new
            {
                UserId = g.Key,
                LatestMessage = g.OrderByDescending(x => x.CreatedAt).First(),
            })
            .ToListAsync(cancellationToken);

        messages1.AddRange(messages2);

        var messages = messages1
            .GroupBy(x => x.UserId)
            .Select(g => new
            {
                UserId = g.Key,
                LatestMessage = g.OrderByDescending(x => x.LatestMessage.CreatedAt).Select(x => x.LatestMessage).First(),
            }).ToList().Select(x => x.LatestMessage).ToList();

        var messageDtos = _mapper.Map<List<MessageDto>>(messages);
        return messageDtos;
    }
}