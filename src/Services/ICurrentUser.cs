﻿using System.Security.Claims;

namespace social_network.src.Services
{
    public interface ICurrentUser
    {
        Guid GetUserId();
    }

    public class CurrentUser : ICurrentUser, ICurrentUserInitializer
    {
        private ClaimsPrincipal? _user;

        public Guid GetUserId()
        {
            Guid userId = Guid.Parse(_user?.Claims.Single(i => i.Type == "Id").Value ?? string.Empty);
            return userId;
        }

        public void SetCurrentUser(ClaimsPrincipal user)
        {
            if (_user != null)
            {
                throw new Exception("Method reserved for in-scope initialization");
            }

            _user = user;
        }
    }
}