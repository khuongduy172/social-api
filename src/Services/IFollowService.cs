﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common.Interfaces;
using social_network.src.Enums;
using social_network.src.Hubs;
using social_network.src.Models;

namespace social_network.src.Services;

public interface IFollowService : ITransientService
{
    Task Follow(Follow follow, CancellationToken cancellationToken = default);

    Task<Follow?> Get(Guid userId, Guid followerId);

    Task Unfollow(Follow follow, CancellationToken cancellationToken = default);
}

public class FollowService : IFollowService
{
    private readonly SocialDbContext _context;
    private readonly IHubContext<CoreHub> _hubContext;

    public FollowService(SocialDbContext context, IHubContext<CoreHub> hubContext)
    {
        _context = context;
        _hubContext = hubContext;
    }

    public async Task Follow(Follow follow, CancellationToken cancellationToken = default)
    {
        _context.Follows.Add(follow);

        var noti = new Notification()
        {
            OwnerId = follow.UserId,
            FromId = follow.FollowerId,
            TypeNoti = TypeNotiEnum.Follow,
        };

        _context.Notifications.Add(noti);

        await _context.SaveChangesAsync(cancellationToken);

        await _hubContext.Clients.Groups(follow.UserId.ToString()).SendAsync("Notification", noti, cancellationToken);
    }

    public async Task<Follow?> Get(Guid userId, Guid followerId)
    {
        return await _context.Follows
            .Include(x => x.Follower)
            .FirstOrDefaultAsync(f => f.UserId == userId && f.FollowerId == followerId);
    }

    public async Task Unfollow(Follow follow, CancellationToken cancellationToken = default)
    {
        _context.Follows.Remove(follow);

        var noti = await _context.Notifications.FirstOrDefaultAsync(x => x.OwnerId == follow.UserId && x.FromId == follow.FollowerId && x.TypeNoti == TypeNotiEnum.Follow, cancellationToken);
        if (noti != null)
        {
            _context.Notifications.Remove(noti);
        }

        await _context.SaveChangesAsync(cancellationToken);
    }
}