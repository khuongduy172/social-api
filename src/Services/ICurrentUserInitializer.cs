﻿using System.Security.Claims;

namespace social_network.src.Services
{
    public interface ICurrentUserInitializer
    {
        void SetCurrentUser(ClaimsPrincipal user);
    }
}