﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Org.BouncyCastle.Ocsp;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using social_network.src.Enums;
using social_network.src.Models;

namespace social_network.src.Services;

public interface IStoryServices : ITransientService
{
    Task Create(CreateStoryRequest request, CancellationToken cancellationToken);

    Task<List<GetStoryResponse>> Get(CancellationToken cancellationToken);

    Task View(Guid storyId, CancellationToken cancellationToken);

    Task Delete(Guid storyId, CancellationToken cancellationToken);
}

public class StoryServices : IStoryServices
{
    private readonly IStorageService _storageService;
    private readonly SocialDbContext _context;
    private readonly ICurrentUser _currentUser;
    private readonly IMapper _mapper;

    public StoryServices(IStorageService storageService, SocialDbContext context, ICurrentUser currentUser, IMapper mapper)
    {
        _storageService = storageService;
        _context = context;
        _currentUser = currentUser;
        _mapper = mapper;
    }

    public async Task Create(CreateStoryRequest request, CancellationToken cancellationToken)
    {
        var type = request.File.ContentType.ToLower().Contains("video")
            ? StoryTypeEnum.Video
            : (request.File.ContentType.ToLower().Contains("image")
                    ? StoryTypeEnum.Image
                    : throw new BadRequestException("invalid file type"));

        var userId = _currentUser.GetUserId();
        var file = await _storageService.UploadAsync(request.File);
        var story = new Story(file.Url, file.FileName, userId, type);

        await _context.Stories.AddAsync(story, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task Delete(Guid storyId, CancellationToken cancellationToken)
    {
        var userId = _currentUser.GetUserId();

        var story = await _context.Stories.FirstOrDefaultAsync(x => x.Id == storyId, cancellationToken) ?? throw new NotFoundException("story not found!");

        if (story.OwnerId != userId)
        {
            throw new ForbiddenException("you are not allow to delete thí story!");
        }

        await _storageService.DeleteAsync(story.Key);

        _context.Stories.Remove(story);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<List<GetStoryResponse>> Get(CancellationToken cancellationToken)
    {
        var userId = _currentUser.GetUserId();
        var followingUser = await _context.Users.Include(x => x.Followings).FirstOrDefaultAsync(x => x.Id == userId, cancellationToken);
        var followingUserIds = followingUser!.Followings.Select(x => x.UserId);

        var stories = await _context.Stories
            .Where(x => (followingUserIds.Contains(x.OwnerId) || x.OwnerId == userId) && x.CreatedAt > DateTime.UtcNow.AddHours(-24))
            .Include(x => x.Owner)
            .Include(x => x.UserViewStory)
            .OrderBy(x => x.CreatedAt)
            .ToListAsync(cancellationToken);

        var dtos = _mapper.Map<List<StoryDto>>(stories);

        var response = dtos
            .GroupBy(x => new { x.OwnerId, x.Owner.Username, x.Owner.Avatar, x.Owner.Name })
            .OrderByDescending(g => g.First().CreatedAt)
            .Select(g => new GetStoryResponse()
            {
                UserId = g.Key.OwnerId,
                UserImage = g.Key.Avatar ?? string.Empty,
                UserName = g.Key.Username ?? g.Key.Name,
                Stories = g.Select(s => s).ToList()
            })
            .ToList();

        return response;
    }

    public async Task View(Guid storyId, CancellationToken cancellationToken)
    {
        var userId = _currentUser.GetUserId();
        var story = await _context.Stories.FirstOrDefaultAsync(x => x.Id == storyId) ?? throw new NotFoundException("story not found!");

        var userViewedStory = await _context.UserViewStories.FirstOrDefaultAsync(x => x.UserId == userId && x.StoryId == storyId, cancellationToken);

        if (userViewedStory == null)
        {
            var userViewStory = new UserViewStory()
            {
                UserId = userId,
                StoryId = storyId,
            };

            await _context.UserViewStories.AddAsync(userViewStory);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}