﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.Enums;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using social_network.src.Enums;
using social_network.src.Hubs;
using social_network.src.Models;

namespace social_network.src.Services;

public interface IStatusService : ITransientService
{
    Task Create(Status status, CancellationToken cancellationToken);

    Task<StatusDto> GetByIdAsync(Guid id, Guid userId);

    Task ReactAsync(Guid statusId, Guid userId, CancellationToken cancellationToken);

    Task<PaginationResponse<CommentDto>> GetCommentsAsync(Guid statusId, Guid userId, PaginationFilter paginationFilter, CancellationToken cancellationToken);

    Task<List<StatusDto>> GetHomeStatusesAsync(Guid userId, CancellationToken cancellationToken);

    Task<CommentDto> AddCommentAsync(Guid userId, Guid statusId, string content, CancellationToken cancellationToken);

    Task DeleteAsync(Guid userId, Guid statusId, CancellationToken cancellationToken);

    Task DeleteCommentAsync(Guid userId, Guid cmtId, CancellationToken cancellationToken);

    Task View(Guid userId, Guid statusId, CancellationToken cancellationToken);
}

public class StatusService : IStatusService
{
    private readonly SocialDbContext _context;
    private readonly IMapper _mapper;
    private readonly IStorageService _storageService;
    private readonly IHubContext<CoreHub> _hubContext;

    public StatusService(SocialDbContext context, IMapper mapper, IStorageService storageService, IHubContext<CoreHub> hubContext)
    {
        _context = context;
        _mapper = mapper;
        _storageService = storageService;
        _hubContext = hubContext;
    }

    public async Task<CommentDto> AddCommentAsync(Guid userId, Guid statusId, string content, CancellationToken cancellationToken)
    {
        var status = await _context.Statuses.FirstOrDefaultAsync(x => x.Id == statusId, cancellationToken) ?? throw new NotFoundException("Status not found!");

        var comment = new Comment(statusId, userId, content);
        _context.Comments.Add(comment);
        await _context.SaveChangesAsync(cancellationToken);

        var newComment = await _context.Comments.Where(c => c.Id == comment.Id).Include(c => c.Owner).FirstAsync(cancellationToken);

        var commentDto = _mapper.Map<CommentDto>(newComment);

        await _hubContext.Clients.Groups(statusId.ToString()).SendAsync(nameof(AddCommentAsync), commentDto, cancellationToken);

        if (userId != status.OwnerId)
        {
            var noti = new Notification()
            {
                OwnerId = status.OwnerId,
                FromId = userId,
                TypeNoti = TypeNotiEnum.Comment,
                StatusId = statusId,
            };

            _context.Notifications.Add(noti);
            await _context.SaveChangesAsync(cancellationToken);

            await _hubContext.Clients.Groups(status.OwnerId.ToString()).SendAsync("Notification", noti, cancellationToken);
        }

        return commentDto;
    }

    public async Task Create(Status status, CancellationToken cancellationToken)
    {
        await _context.Statuses.AddAsync(status);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteAsync(Guid userId, Guid statusId, CancellationToken cancellationToken)
    {
        var status = await _context.Statuses
            .Include(s => s.StatusImages)
            .FirstOrDefaultAsync(s => s.Id == statusId)
            ?? throw new NotFoundException("Status not found!");

        if (status.OwnerId != userId)
        {
            throw new ForbiddenException("You can not take this action!");
        }

        foreach (var image in status.StatusImages)
        {
            await _storageService.DeleteAsync(image.Name);
        }

        var notifications = await _context.Notifications.Where(x => x.StatusId == statusId).ToListAsync(cancellationToken);
        if (notifications.Count > 0)
        {
            _context.Notifications.RemoveRange(notifications);
        }

        _context.Statuses.Remove(status);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteCommentAsync(Guid userId, Guid cmtId, CancellationToken cancellationToken)
    {
        var cmt = await _context.Comments
            .FirstOrDefaultAsync(s => s.Id == cmtId)
            ?? throw new NotFoundException("Comment not found!");

        if (cmt.OwnerId != userId)
        {
            throw new ForbiddenException("You can not take this action!");
        }

        _context.Comments.Remove(cmt);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<StatusDto> GetByIdAsync(Guid id, Guid userId)
    {
        var status = await _context.Statuses
            .Include(x => x.StatusImages)
            .Include(x => x.Owner)
            .Include(x => x.Reacts)
            .Include(x => x.Comments)
            .FirstOrDefaultAsync(x => x.Id == id) ?? throw new NotFoundException("Status not found");
        var react = await _context.Reacts.FirstOrDefaultAsync(x => x.UserId == userId && x.StatusId == id);

        var statusDtos = _mapper.Map<StatusDto>(status);

        if (react != null)
        {
            statusDtos.IsReacted = true;
        }

        if (statusDtos.OwnerId == userId)
        {
            statusDtos.IsOwner = true;
        }

        return statusDtos;
    }

    public async Task<PaginationResponse<CommentDto>> GetCommentsAsync(Guid statusId, Guid userId, PaginationFilter paginationFilter, CancellationToken cancellationToken)
    {
        var status = await _context.Statuses.FirstOrDefaultAsync(s => s.Id == statusId) ?? throw new NotFoundException("Status not found!");
        var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId) ?? throw new NotFoundException("User not found!");

        var query = _context.Comments
            .Where(c => c.StatusId == statusId)
            .Include(c => c.Owner)
            .OrderByDescending(c => c.CreatedAt);
        var comments = await query
            .Skip((paginationFilter.PageNumber - 1) * paginationFilter.PageSize)
            .Take(paginationFilter.PageSize)
            .ToListAsync();
        var count = await query.CountAsync();
        var commentDtos = _mapper.Map<List<CommentDto>>(comments);
        foreach (var dto in commentDtos)
        {
            if (dto.OwnerId == userId)
            {
                dto.IsOwner = true;
            }
        }
        return new PaginationResponse<CommentDto>(commentDtos, count, paginationFilter.PageNumber, paginationFilter.PageSize);
    }

    public async Task<List<StatusDto>> GetHomeStatusesAsync(Guid userId, CancellationToken cancellationToken)
    {
        var viewedStatusIds = await _context.UserViewStatuses.Where(x => x.UserId == userId).Select(x => x.StatusId).ToListAsync(cancellationToken);
        var followingUser = await _context.Users.Include(x => x.Followings).FirstOrDefaultAsync(x => x.Id == userId);
        var followingUserIds = followingUser!.Followings.Select(x => x.UserId);

        var query = _context.Statuses.Where(x => (!viewedStatusIds.Contains(x.Id) && followingUserIds.Any(y => y == x.OwnerId))
                                                    || (x.OwnerId == userId && x.CreatedAt > DateTime.UtcNow.AddMinutes(-15)));

        #region query status

        //var status = await query
        //    .Include(x => x.StatusImages)
        //    .Include(x => x.Owner)
        //    .Include(x => x.Reacts)
        //    .Include(x => x.Comments)
        //    .OrderByDescending(x => x.CreatedAt)
        //    .AsSplitQuery()
        //    .AsNoTracking()
        //    .Take(10).ToListAsync(cancellationToken);

        //if (status.Count == 0)
        //{
        //    query = _context.Statuses.Where(x => viewedStatusIds.Contains(x.Id));

        //    status = await query
        //        .Include(x => x.StatusImages)
        //        .Include(x => x.Owner)
        //        .Include(x => x.Reacts)
        //        .Include(x => x.Comments)
        //        .OrderByDescending(x => Guid.NewGuid())
        //        .AsSplitQuery()
        //        .AsNoTracking()
        //        .Take(10).ToListAsync(cancellationToken);
        //}

        //if (status.Count == 0)
        //{
        //    status = await _context.Statuses
        //        .Include(x => x.StatusImages)
        //        .Include(x => x.Owner)
        //        .Include(x => x.Reacts)
        //        .Include(x => x.Comments)
        //        .OrderByDescending(x => Guid.NewGuid())
        //        .AsSplitQuery()
        //        .AsNoTracking()
        //        .Take(10).ToListAsync(cancellationToken);
        //}

        #endregion query status

        var status = await query
            .Include(x => x.StatusImages)
            .Include(x => x.Owner)
            .Include(x => x.Reacts)
            .Include(x => x.Comments)
            .OrderByDescending(x => x.CreatedAt)
            //.AsSplitQuery()
            //.AsNoTracking()
            .ToListAsync(cancellationToken);

        query = _context.Statuses.Where(x => viewedStatusIds.Contains(x.Id));

        var statusViewed = await query
            .Include(x => x.StatusImages)
            .Include(x => x.Owner)
            .Include(x => x.Reacts)
            .Include(x => x.Comments)
            .OrderByDescending(x => Guid.NewGuid())
            //.AsSplitQuery()
            //.AsNoTracking()
            .ToListAsync(cancellationToken);

        status.AddRange(statusViewed);

        if (status.Count == 0)
        {
            status = await _context.Statuses
                .Include(x => x.StatusImages)
                .Include(x => x.Owner)
                .Include(x => x.Reacts)
                .Include(x => x.Comments)
                .OrderByDescending(x => Guid.NewGuid())
                //.AsSplitQuery()
                //.AsNoTracking()
                .Take(20).ToListAsync(cancellationToken);
        }

        var statusDtos = _mapper.Map<List<StatusDto>>(status);

        foreach (var dto in statusDtos)
        {
            var react = await _context.Reacts.FirstOrDefaultAsync(x => x.UserId == userId && x.StatusId == dto.Id);

            if (react != null)
            {
                dto.IsReacted = true;
            }

            if (dto.OwnerId == userId)
            {
                dto.IsOwner = true;
            }
        }

        return statusDtos;
    }

    public async Task ReactAsync(Guid statusId, Guid userId, CancellationToken cancellationToken)
    {
        var status = await _context.Statuses.FirstOrDefaultAsync(s => s.Id == statusId) ?? throw new NotFoundException("Status not found!");
        var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId) ?? throw new NotFoundException("User not found!");
        var react = await _context.Reacts.FirstOrDefaultAsync(r => r.UserId == userId && r.StatusId == statusId);
        if (react == null)
        {
            react = new React(statusId, userId, ReactEnums.Love.ToString());
            _context.Reacts.Add(react);

            if (userId != status.OwnerId)
            {
                var noti = new Notification()
                {
                    OwnerId = status.OwnerId,
                    FromId = userId,
                    TypeNoti = TypeNotiEnum.React,
                    StatusId = statusId,
                };

                _context.Notifications.Add(noti);
                await _context.SaveChangesAsync(cancellationToken);

                await _hubContext.Clients.Groups(status.OwnerId.ToString()).SendAsync("Notification", noti, cancellationToken);
            }
        }
        else
        {
            _context.Remove(react);
            var noti = await _context.Notifications.FirstOrDefaultAsync(x => x.OwnerId == status.OwnerId && x.StatusId == statusId && x.FromId == userId, cancellationToken);
            if (noti != null)
            {
                _context.Notifications.Remove(noti);
            }
        }
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task View(Guid userId, Guid statusId, CancellationToken cancellationToken)
    {
        var status = await _context.Statuses.FirstOrDefaultAsync(x => x.Id == statusId, cancellationToken) ?? throw new NotFoundException("status not found!");

        var userViewedStatus = await _context.UserViewStatuses.FirstOrDefaultAsync(x => x.UserId == userId && x.StatusId == statusId, cancellationToken);

        if (userViewedStatus == null)
        {
            var userViewStatus = new UserViewStatus()
            {
                UserId = userId,
                StatusId = statusId,
            };

            await _context.AddAsync(userViewStatus);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}