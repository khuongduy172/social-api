﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using social_network.src.Models;

namespace social_network.src.Services;

public interface IStatusImageService : ITransientService
{
    Task Create(StatusImage statusImage, CancellationToken cancellationToken);

    Task<List<RandomImagesDto>> GetRandomImagesAsync();

    Task<PaginationResponse<StatusImageDto>> GetByUserId(Guid userId, PaginationFilter paginationFilter);
}

public class StatusImageService : IStatusImageService
{
    private readonly SocialDbContext _context;
    private readonly IMapper _mapper;

    public StatusImageService(SocialDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task Create(StatusImage statusImage, CancellationToken cancellationToken)
    {
        await _context.StatusImages.AddAsync(statusImage, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
    }

    public async Task<PaginationResponse<StatusImageDto>> GetByUserId(Guid userId, PaginationFilter paginationFilter)
    {
        var query = _context.StatusImages
            .Include(s => s.Status)
            .Where(s => s.Status.OwnerId == userId);
        var result = await query
            .Skip((paginationFilter.PageNumber - 1) * paginationFilter.PageSize)
            .Take(paginationFilter.PageSize)
            .ToListAsync();
        var dtos = _mapper.Map<List<StatusImageDto>>(result);
        int count = await query.CountAsync();
        return new PaginationResponse<StatusImageDto>(dtos, count, paginationFilter.PageNumber, paginationFilter.PageSize);
    }

    public async Task<List<RandomImagesDto>> GetRandomImagesAsync()
    {
        var images = await _context.StatusImages
            .OrderBy(s => EF.Functions.Random())
            .Take(100)
            .ToListAsync();
        var randomImagesDto = _mapper.Map<List<RandomImagesDto>>(images);
        return randomImagesDto;
    }
}