﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Options;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;
using System.Text.RegularExpressions;

namespace social_network.src.Services
{
    public interface IStorageService : ITransientService
    {
        Task<ImageObject> UploadAsync(IFormFile file);

        Task<ImageObject> UploadAsync(string path, string contentType);

        Task DeleteAsync(string fileName);
    }

    public class StorageService : IStorageService
    {
        private readonly AmazonS3Client _s3Client;
        private readonly string _bucketName = "19420200-instagram";

        public StorageService(IOptions<AWSCredentials> awsCredentials)
        {
            var credentials = new BasicAWSCredentials(awsCredentials.Value.AccessKey, awsCredentials.Value.SecretKey);

            var config = new AmazonS3Config()
            {
                ServiceURL = awsCredentials.Value.RegionEndpoint
            };
            _s3Client = new AmazonS3Client(credentials, config);
        }

        public async Task DeleteAsync(string fileName)
        {
            var deleteRequest = new DeleteObjectRequest
            {
                BucketName = _bucketName,
                Key = fileName
            };

            await _s3Client.DeleteObjectAsync(deleteRequest);
        }

        public async Task<ImageObject> UploadAsync(IFormFile file)
        {
            using var stream = new MemoryStream();
            file.CopyTo(stream);
            string fileName = Regex.Replace(DateTime.UtcNow.ToString() + file.FileName, @"[^0-9a-zA-Z\._]", "");
            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = stream,
                Key = fileName,
                BucketName = _bucketName,
                ContentType = file.ContentType,
            };

            var fileTransferUtility = new TransferUtility(_s3Client);
            await fileTransferUtility.UploadAsync(uploadRequest);

            GetPreSignedUrlRequest request = new()
            {
                BucketName = _bucketName,
                Key = fileName,
                Verb = HttpVerb.GET,
                Expires = DateTime.UtcNow.AddYears(10)
            };

            string preSignedUrl = _s3Client.GetPreSignedURL(request);
            return new ImageObject()
            {
                FileName = fileName,
                Url = preSignedUrl,
            };
        }

        public async Task<ImageObject> UploadAsync(string path, string contentType)
        {
            using var stream = File.Open(path, FileMode.Open);
            string fileName = Regex.Replace(DateTime.UtcNow.ToString() + Path.GetFileName(path), @"[^0-9a-zA-Z\._]", "");
            var uploadRequest = new TransferUtilityUploadRequest
            {
                InputStream = stream,
                Key = fileName,
                BucketName = _bucketName,
                ContentType = contentType,
            };

            var fileTransferUtility = new TransferUtility(_s3Client);
            await fileTransferUtility.UploadAsync(uploadRequest);

            GetPreSignedUrlRequest request = new()
            {
                BucketName = _bucketName,
                Key = fileName,
                Verb = HttpVerb.GET,
                Expires = DateTime.UtcNow.AddYears(10)
            };

            string preSignedUrl = _s3Client.GetPreSignedURL(request);

            File.Delete(path);

            return new ImageObject()
            {
                FileName = fileName,
                Url = preSignedUrl,
            };
        }
    }

    public class AWSCredentials
    {
        public string? AccessKey { get; set; }
        public string? SecretKey { get; set; }
        public string? RegionEndpoint { get; set; }
    }
}