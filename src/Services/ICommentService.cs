﻿using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;

namespace social_network.src.Services
{
    public interface ICommentService : ITransientService
    {
        Task Delete(Guid id, CancellationToken cancellationToken);
    }

    public class CommentService : ICommentService
    {
        private readonly SocialDbContext _context;
        private readonly ICurrentUser _currentUser;

        public CommentService(SocialDbContext context, ICurrentUser currentUser)
        {
            _context = context;
            _currentUser = currentUser;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == id) ?? throw new NotFoundException("Comment not found!");
            var userId = _currentUser.GetUserId();
            if (comment.OwnerId != userId)
            {
                throw new ForbiddenException("You can't take this action!");
            }
            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}