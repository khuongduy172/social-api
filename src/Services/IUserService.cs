using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Common.Mailing;
using social_network.src.Common.Mailing.MailModels;
using social_network.src.Models;
using System.Text;

namespace social_network.src.Services;

public interface IUserService : ITransientService
{
    Task<IEnumerable<User>> GetAllUser();

    Task<User?> GetUserAsync(Guid userId);

    Task<User?> GetUserByEmailAsync(string email);

    Task<User?> GetUserByFacebookUserIdAsync(string facebookUserId);

    Task<User?> GetUser(string email, string password);

    Task<User> CreateUser(User user, CancellationToken cancellationToken);

    Task<User> UpdateUser(User user, CancellationToken cancellationToken);

    Task<User?> DeleteUser(Guid userId, CancellationToken cancellationToken);

    Task<List<User>> SearchUser(string keyword, Guid userId);

    Task ResetPassword(string email, CancellationToken cancellationToken);
}

public class UserService : IUserService
{
    private readonly SocialDbContext _context;
    private readonly IMailService _mailService;

    public UserService(SocialDbContext context, IMailService mailService)
    {
        _context = context;
        _mailService = mailService;
    }

    public async Task<IEnumerable<User>> GetAllUser()
    {
        return await _context.Users.ToListAsync();
    }

    public async Task<User?> GetUserAsync(Guid userId)
    {
        return await _context.Users
            .Include(u => u.Statuses)
            .ThenInclude(s => s.StatusImages)
            .Include(u => u.Followers)
            .Include(u => u.Followings)
            .Include(u => u.Reels)
            .AsSplitQuery()
            .FirstOrDefaultAsync(u => u.Id == userId);
    }

    public async Task<User?> GetUser(string email, string password)
    {
        var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        if (user != null && BCrypt.Net.BCrypt.Verify(password, user.Password))
        {
            return user;
        }
        return null;
    }

    public async Task<User> CreateUser(User user, CancellationToken cancellationToken)
    {
        user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password);
        await _context.Users.AddAsync(user, cancellationToken);
        await _context.SaveChangesAsync(cancellationToken);
        return user;
    }

    public async Task<User> UpdateUser(User user, CancellationToken cancellationToken)
    {
        _context.Users.Update(user);
        await _context.SaveChangesAsync(cancellationToken);
        return user;
    }

    public async Task<User?> DeleteUser(Guid userId, CancellationToken cancellationToken)
    {
        var user = await _context.Users.FindAsync(userId);
        if (user == null)
        {
            return null;
        }
        _context.Users.Remove(user);
        await _context.SaveChangesAsync(cancellationToken);
        return user;
    }

    public async Task<List<User>> SearchUser(string keyword, Guid userId)
    {
        var users = await _context.Users
            .Where(u => u.Id != userId && (u.Name.ToLower().Contains(keyword.ToLower()) || u.Username.ToLower().Contains(keyword.ToLower())))
            .Include(u => u.Followers)
            .ToListAsync();
        return users;
    }

    public async Task<User?> GetUserByEmailAsync(string email)
    {
        return await _context.Users
            .FirstOrDefaultAsync(u => u.Email == email);
    }

    public async Task<User?> GetUserByFacebookUserIdAsync(string facebookUserId)
    {
        return await _context.Users
            .FirstOrDefaultAsync(u => u.FacebookUserId == facebookUserId);
    }

    public async Task ResetPassword(string email, CancellationToken cancellationToken)
    {
        var user = await GetUserByEmailAsync(email) ?? throw new NotFoundException("User not found!");
        string newPassword = RandomPassword();
        user.Password = BCrypt.Net.BCrypt.HashPassword(newPassword);
        _context.Users.Update(user);
        await _context.SaveChangesAsync(cancellationToken);

        string emailTemplate = _mailService.GenerateEmailTemplate("reset-password", new PasswordResetMailModel { Email = email, Password = newPassword });
        var mailData = new MailData(new List<string> { email }, "Password reset from Instagram", emailTemplate);
        await _mailService.SendAsync(mailData);
    }

    #region random

    private readonly Random _random = new Random();

    // Generates a random number within a range.
    public int RandomNumber(int min, int max)
    {
        return _random.Next(min, max);
    }

    public string RandomString(int size, bool lowerCase = false)
    {
        var builder = new StringBuilder(size);

        // Unicode/ASCII Letters are divided into two blocks
        // (Letters 65�90 / 97�122):
        // The first group containing the uppercase letters and
        // the second group containing the lowercase.

        // char is a single Unicode character
        char offset = lowerCase ? 'a' : 'A';
        const int lettersOffset = 26; // A...Z or a..z: length=26

        for (var i = 0; i < size; i++)
        {
            var @char = (char)_random.Next(offset, offset + lettersOffset);
            builder.Append(@char);
        }

        return lowerCase ? builder.ToString().ToLower() : builder.ToString();
    }

    public string RandomPassword()
    {
        var passwordBuilder = new StringBuilder();

        // 4-Letters lower case
        passwordBuilder.Append(RandomString(4, true));

        // 4-Digits between 1000 and 9999
        passwordBuilder.Append(RandomNumber(1000, 9999));

        // 2-Letters upper case
        passwordBuilder.Append(RandomString(2));
        return passwordBuilder.ToString();
    }

    #endregion random
}