﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Common;
using social_network.src.Common.Interfaces;
using social_network.src.Dtos;

namespace social_network.src.Services;

public interface INotiService : ITransientService
{
    Task<PaginationResponse<NotiDto>> GetAsync(PaginationFilter paginationFilter);

    Task Read(Guid notiId, CancellationToken cancellationToken);
}

public class NotiService : INotiService
{
    private readonly SocialDbContext _context;
    private readonly IMapper _mapper;
    private readonly ICurrentUser _currentUser;

    public NotiService(SocialDbContext context, IMapper mapper, ICurrentUser currentUser)
    {
        _context = context;
        _mapper = mapper;
        _currentUser = currentUser;
    }

    public async Task<PaginationResponse<NotiDto>> GetAsync(PaginationFilter paginationFilter)
    {
        var userId = _currentUser.GetUserId();
        var query = _context.Notifications
            .Where(n => n.OwnerId == userId)
            .OrderByDescending(n => n.CreatedAt)
            .Include(n => n.FromUser);
        var result = await query
            .Skip((paginationFilter.PageNumber - 1) * paginationFilter.PageSize)
            .Take(paginationFilter.PageSize)
            .ToListAsync();
        int count = await query.CountAsync();

        var dtos = _mapper.Map<List<NotiDto>>(result);
        return new PaginationResponse<NotiDto>(dtos, count, paginationFilter.PageNumber, paginationFilter.PageSize);
    }

    public async Task Read(Guid notiId, CancellationToken cancellationToken)
    {
        var noti = await _context.Notifications.FirstOrDefaultAsync(n => n.Id == notiId, cancellationToken) ?? throw new NotFoundException("Notification not found!");
        var userId = _currentUser.GetUserId();
        if (noti.OwnerId != userId)
        {
            throw new ForbiddenException("You are not allow to read this noti!");
        }

        noti.IsRead = true;
        _context.Notifications.Update(noti);
        await _context.SaveChangesAsync(cancellationToken);
    }
}