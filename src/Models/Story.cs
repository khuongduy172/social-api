﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using social_network.src.Models.Common;
using social_network.src.Enums;

namespace social_network.src.Models
{
    public class Story : Auditable
    {
        public Story(string url, string key, Guid ownerId, StoryTypeEnum type)
        {
            Url = url;
            Key = key;
            OwnerId = ownerId;
            Type = type;
        }

        public string Url { get; set; }
        public string Key { get; set; }
        public StoryTypeEnum Type { get; set; }
        public Guid OwnerId { get; set; }

        public virtual User Owner { get; set; } = default!;
        public virtual ICollection<UserViewStory> UserViewStory { get; set; } = default!;
    }

    public class StoryConfig : IEntityTypeConfiguration<Story>
    {
        public void Configure(EntityTypeBuilder<Story> builder)
        {
            builder.ToTable("Story");
            builder.HasKey(c => c.Id);
            builder.HasOne(s => s.Owner).WithMany(u => u.Stories).HasForeignKey(s => s.OwnerId);
            builder.HasMany(s => s.UserViewStory).WithOne(us => us.Story).HasForeignKey(s => s.StoryId);
        }
    }
}