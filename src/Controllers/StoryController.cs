﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Dtos;
using social_network.src.Services;

namespace social_network.src.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StoryController : ControllerBase
{
    private readonly IStoryServices _storyServices;

    public StoryController(IStoryServices storyServices)
    {
        _storyServices = storyServices;
    }

    [HttpPost]
    [Authorize]
    public async Task<ActionResult> Create([FromForm] CreateStoryRequest request, CancellationToken cancellationToken)
    {
        await _storyServices.Create(request, cancellationToken);
        return Ok("ok");
    }

    [HttpGet]
    [Authorize]
    public async Task<ActionResult<List<GetStoryResponse>>> Get(CancellationToken cancellationToken)
    {
        return await _storyServices.Get(cancellationToken);
    }

    [HttpPost("{id:guid}/view")]
    [Authorize]
    public async Task<ActionResult> ViewReel(Guid id, CancellationToken cancellationToken)
    {
        await _storyServices.View(id, cancellationToken);
        return Ok(id);
    }

    [HttpDelete("{id:guid}")]
    [Authorize]
    public async Task<ActionResult<Guid>> Delete(Guid id, CancellationToken cancellationToken)
    {
        await _storyServices.Delete(id, cancellationToken);
        return Ok(id);
    }
}