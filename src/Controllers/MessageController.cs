﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Services;

namespace social_network.src.Controllers;

[Route("api/[controller]")]
[ApiController]
public class MessageController : ControllerBase
{
    private readonly IMessageService _messageService;

    public MessageController(IMessageService messageService)
    {
        _messageService = messageService;
    }

    [HttpPost]
    [Authorize]
    public async Task<IActionResult> SendMessage([FromForm] SendMessageRequest request, CancellationToken cancellationToken)
    {
        await _messageService.Create(request, cancellationToken);
        return Ok();
    }

    [HttpGet("user/{userId:guid}")]
    [Authorize]
    public async Task<PaginationResponse<MessageDto>> GetUserMessage(Guid userId, [FromQuery] PaginationFilter filter, CancellationToken cancellationToken)
    {
        return await _messageService.Get(userId, filter, cancellationToken);
    }

    [HttpGet("list")]
    [Authorize]
    public async Task<List<MessageDto>> GetUserMessageList(CancellationToken cancellationToken)
    {
        return await _messageService.GetUserList(cancellationToken);
    }
}