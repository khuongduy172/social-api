﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Services;

namespace social_network.src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotiController : ControllerBase
    {
        private readonly INotiService _notiService;

        public NotiController(INotiService notiService)
        {
            _notiService = notiService;
        }

        [HttpGet]
        [Authorize]
        public async Task<PaginationResponse<NotiDto>> GetNoti([FromQuery] PaginationFilter paginationFilter)
        {
            return await _notiService.GetAsync(paginationFilter);
        }

        [HttpPut("{id:guid}/read")]
        [Authorize]
        public async Task<ActionResult> Read(Guid id, CancellationToken cancellationToken)
        {
            await _notiService.Read(id, cancellationToken);
            return Ok();
        }
    }
}