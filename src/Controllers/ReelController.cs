﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Models;
using social_network.src.Services;
using System.IO;

namespace social_network.src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReelController : ControllerBase
    {
        private readonly ICurrentUser _currentUser;
        private readonly IReelService _reelService;
        private readonly IStorageService _storageService;

        public ReelController(ICurrentUser currentUser, IReelService reelService, IStorageService storageService)
        {
            _currentUser = currentUser;
            _reelService = reelService;
            _storageService = storageService;
        }

        [HttpPost]
        [Authorize]
        [RequestSizeLimit(100_000_000)]
        public async Task<ActionResult> CreateReel([FromForm] CreateReelRequest request, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();

            if (request.Video == null)
            {
                throw new BadRequestException("can not create null reel!");
            }

            var video = await _storageService.UploadAsync(request.Video);

            if (video == null)
            {
                throw new CustomException("upload video failed!");
            }

            //var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            //var folderName = Path.Combine("Resources", "Images"); // in source code
            //var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            //bool folderExists = Directory.Exists(pathToSave);
            //if (!folderExists)
            //    Directory.CreateDirectory(pathToSave);
            //var fullPath = Path.Combine(pathToSave, "video_thumbnail.jpg");
            //ffMpeg.GetVideoThumbnail(video.Url, fullPath);

            //var thumbnail = await _storageService.UploadAsync(fullPath, "image/jpeg");

            var reel = new Reel()
            {
                Caption = request.Caption,
                Key = video.FileName,
                Url = video.Url,
                OwnerId = userId,
                //ThumbnailUrl = thumbnail.Url,
                //ThumbnailKey = thumbnail.FileName,
            };

            await _reelService.Create(reel, cancellationToken);

            return Ok(reel);
        }

        [HttpPost("{id:guid}/view")]
        [Authorize]
        public async Task<ActionResult> ViewReel(Guid id, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _reelService.View(userId, id, cancellationToken);
            return Ok(id);
        }

        [HttpPost("{id:guid}/react")]
        [Authorize]
        public async Task<ActionResult> React(Guid id, CancellationToken cancellationToken)
        {
            await _reelService.ReactAsync(id, cancellationToken);
            return Ok(id);
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<ReelDto>>> GetReel(CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            var reels = await _reelService.Get(userId, cancellationToken);
            return Ok(reels);
        }

        [HttpDelete("{id:guid}")]
        [Authorize]
        public async Task<ActionResult<Guid>> DeleteReel(Guid id, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _reelService.Delete(userId, id, cancellationToken);
            return Ok(id);
        }

        [HttpGet("user/{userId:guid}")]
        [Authorize]
        public async Task<ActionResult<List<ReelDto>>> GetUserReel(Guid userId, CancellationToken cancellationToken)
        {
            var currentUserId = _currentUser.GetUserId();
            var result = await _reelService.GetUser(userId, currentUserId, cancellationToken);
            return Ok(result);
        }
    }
}