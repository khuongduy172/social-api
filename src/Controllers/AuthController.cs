using Microsoft.AspNetCore.Mvc;
using social_network.Dtos;
using System.Security.Claims;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using social_network.src.Dtos;
using Google.Apis.Auth;
using social_network.src.Common;
using System.Net;
using System.Security.Cryptography;
using social_network.src.Enums;
using Newtonsoft.Json;
using social_network.src.Models;
using social_network.src.Services;

namespace social_network.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AuthController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
    private readonly IConfiguration _configuration;
    private readonly IUserService _userService;

    public AuthController(ILogger<UserController> logger, IUserService userService, IConfiguration configuration)
    {
        _logger = logger;
        _userService = userService;
        _configuration = configuration;
    }

    [HttpPost("register")]
    public async Task<ActionResult> Register(RegisterRequest request, CancellationToken cancellationToken)
    {
        if (request.Password != request.PasswordConfirm)
        {
            return BadRequest("Passwords do not match");
        }

        string gender = request.IsMale ? "Male" : "Female";
        var user = new User(request.Username, request.Password, request.Email, request.Name, new DateTime(2001, 01, 01), gender);

        try
        {
            await _userService.CreateUser(user, cancellationToken);
        }
        catch
        {
            return BadRequest(new { message = "Email already exists" });
        }

        return CreatedAtAction(nameof(Register), new { message = "Register successfully" });
    }

    [HttpPut("password-reset")]
    public async Task<ActionResult> PasswordReset(PasswordResetRequest request, CancellationToken cancellationToken)
    {
        await _userService.ResetPassword(request.Email, cancellationToken);
        return Ok(request.Email);
    }

    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginRequest request, CancellationToken cancellationToken)
    {
        var user = await _userService.GetUser(request.Email, request.Password) ?? throw new CustomException("Wrong email/password", null, HttpStatusCode.Unauthorized);

        var result = await CreateAccessAndRefreshToken(user, cancellationToken);
        return Ok(result);
    }

    [HttpPost("refresh")]
    public async Task<IActionResult> RefreshToken(TokenModel tokenModel, CancellationToken cancellationToken)
    {
        var principal = GetPrincipalFromExpiredToken(tokenModel.Token);
        if (principal == null)
        {
            return BadRequest("Invalid access token or refresh token");
        }

        string userId = principal.Claims.Single(i => i.Type == "Id").Value ?? string.Empty;
        var user = await _userService.GetUserAsync(new Guid(userId));

        if (user == null || user.RefreshToken != tokenModel.RefreshToken || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
        {
            return BadRequest("Invalid access token or refresh token");
        }

        var result = await CreateAccessAndRefreshToken(user, cancellationToken);
        return Ok(result);
    }

    [HttpPost("external-login")]
    public async Task<IActionResult> ExternalLogin([FromBody] ExternalAuthDto request, CancellationToken cancellationToken)
    {
        if (request.Provider == nameof(ExternalAuthProviderEnum.Google))
        {
            var settings = new GoogleJsonWebSignature.ValidationSettings() { };
            var payload = await GoogleJsonWebSignature.ValidateAsync(request.IdToken, settings);

            var user = await _userService.GetUserByEmailAsync(payload.Email);
            if (user == null)
            {
                user = new User(string.Empty, string.Empty, payload.Email, payload.Name, new DateTime(2001, 01, 01), "Male");
                await _userService.CreateUser(user, cancellationToken);
            }

            var result = await CreateAccessAndRefreshToken(user, cancellationToken);
            return Ok(result);
        }

        if (request.Provider == nameof(ExternalAuthProviderEnum.Facebook))
        {
            var payload = await VerifyFacebookAccessToken(request.IdToken ?? string.Empty) ?? throw new CustomException("Unauthorized", null, HttpStatusCode.Unauthorized);

            var user = await _userService.GetUserByFacebookUserIdAsync(payload.Id);
            if (user == null)
            {
                user = new User(payload.Id, payload.LastName + " " + payload.FirstName, new DateTime(2001, 01, 01), "Male");
                await _userService.CreateUser(user, cancellationToken);
            }

            var result = await CreateAccessAndRefreshToken(user, cancellationToken);
            return Ok(result);
        }
        return BadRequest();
    }

    private async Task<AuthResponse> CreateAccessAndRefreshToken(User user, CancellationToken cancellationToken)
    {
        var token = CreateToken(user);

        var refreshToken = GenerateRefreshToken();
        user.RefreshToken = refreshToken;
        user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(_configuration.GetValue<int>("Jwt:RefreshTokenValidityInDays"));
        await _userService.UpdateUser(user, cancellationToken);

        return new AuthResponse
        {
            UserId = user.Id,
            Token = new JwtSecurityTokenHandler().WriteToken(token).ToString(),
            RefreshToken = refreshToken,
            Expiration = token.ValidTo,
        };
    }

    private JwtSecurityToken CreateToken(User user)
    {
        //create claims details based on the user information
        var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email ?? string.Empty),
                   };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

        var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        return new JwtSecurityToken(
            _configuration["Jwt:Issuer"],
            _configuration["Jwt:Audience"],
            claims,
            expires: DateTime.UtcNow.AddMinutes(_configuration.GetValue<int>("Jwt:TokenValidityInMinutes")),
            signingCredentials: signIn);
    }

    private string GenerateRefreshToken()
    {
        var randomNumber = new byte[64];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }

    private ClaimsPrincipal? GetPrincipalFromExpiredToken(string? token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])),
            ValidateLifetime = false
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
        if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            throw new SecurityTokenException("Invalid token");

        return principal;
    }

    private async Task<FacebookUser?> VerifyFacebookAccessToken(string accessToken)
    {
        var client = new HttpClient();
        var uri = $"https://graph.facebook.com/me?access_token={accessToken}&fields=id,email,first_name,last_name";
        var response = await client.GetAsync(uri);
        if (!response.IsSuccessStatusCode)
        {
            return null;
        }

        var content = await response.Content.ReadAsStringAsync();
        var facebookUser = JsonConvert.DeserializeObject<FacebookUser>(content);
        return facebookUser;
    }
}