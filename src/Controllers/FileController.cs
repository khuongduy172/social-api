using Microsoft.AspNetCore.Mvc;
using social_network.src.Services;

namespace social_network.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FileController : ControllerBase
{
    private readonly IStorageService _storageService;

    public FileController(IStorageService storageService)
    {
        _storageService = storageService;
    }

    [HttpPost]
    public async Task<IActionResult> OnPostUploadAsync(IFormFile file)
    {
        var result = await _storageService.UploadAsync(file);

        return Ok(result);
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(string fileName)
    {
        await _storageService.DeleteAsync(fileName);
        return Ok();
    }
}