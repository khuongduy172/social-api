﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Services;

namespace social_network.src.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CommentController : ControllerBase
{
    private readonly ICurrentUser _currentUser;
    private readonly ICommentService _commentService;

    public CommentController(ICurrentUser currentUser, ICommentService commentService)
    {
        _currentUser = currentUser;
        _commentService = commentService;
    }

    [HttpGet("{id:guid}/delete")]
    [Authorize]
    public async Task Delete(Guid id, CancellationToken cancellationToken)
    {
        await _commentService.Delete(id, cancellationToken);
    }
}