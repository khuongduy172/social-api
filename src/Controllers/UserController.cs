using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Enums;
using social_network.src.Services;

namespace social_network.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
    private readonly IUserService _userService;
    private readonly ICurrentUser _currentUser;
    private readonly IMapper _mapper;
    private readonly IStorageService _storageService;

    public UserController(ILogger<UserController> logger, IUserService userService, ICurrentUser currentUser, IMapper mapper, IStorageService storageService)
    {
        _logger = logger;
        _userService = userService;
        _currentUser = currentUser;
        _mapper = mapper;
        _storageService = storageService;
    }

    [HttpGet("{id:guid}")]
    [Authorize]
    public async Task<UserDto> GetUserById(Guid id)
    {
        var user = await _userService.GetUserAsync(id) ?? throw new NotFoundException("User not found!");
        UserDto userDto = _mapper.Map<UserDto>(user);
        var userId = _currentUser.GetUserId();
        if (userId == id)
        {
            userDto.IsOwner = true;
        }

        if (userDto.Followings.Any(f => f.UserId == userId))
        {
            userDto.FollowStatus = nameof(FollowStatusEnums.FollowedBy);
        }

        if (userDto.Followers.Any(f => f.FollowerId == userId))
        {
            userDto.FollowStatus = nameof(FollowStatusEnums.Following);
        }
        return userDto;
    }

    [Authorize]
    [HttpGet("me")]
    public async Task<UserDto> GetMe()
    {
        var userId = _currentUser.GetUserId();
        var user = await _userService.GetUserAsync(userId) ?? throw new NotFoundException("User not found!");
        UserDto userDto = _mapper.Map<UserDto>(user);
        userDto.IsOwner = true;
        return userDto;
    }

    [Authorize]
    [HttpPut]
    public async Task<UserDto> UpdateProfile(UpdateUserProfileRequest request, CancellationToken cancellationToken)
    {
        var userId = _currentUser.GetUserId();
        var user = await _userService.GetUserAsync(userId) ?? throw new NotFoundException("User not found!");
        user.Update(request.Bio, request.Name, request.DayOfBirth, request.Gender, request.Phone, request.Username);
        await _userService.UpdateUser(user, cancellationToken);
        UserDto userDto = _mapper.Map<UserDto>(user);
        userDto.IsOwner = true;
        return userDto;
    }

    [Authorize]
    [HttpPost("avatar")]
    public async Task<UserDto> UpdateAvatar([FromForm] UpdateAvatarRequest request, CancellationToken cancellationToken)
    {
        var userId = _currentUser.GetUserId();
        var user = await _userService.GetUserAsync(userId) ?? throw new NotFoundException("User not found!");

        if (user.AvatarKey != null)
        {
            await _storageService.DeleteAsync(user.AvatarKey);
        }

        var image = await _storageService.UploadAsync(request.File);

        user.Update(image.Url, image.FileName);
        await _userService.UpdateUser(user, cancellationToken);
        UserDto userDto = _mapper.Map<UserDto>(user);
        userDto.IsOwner = true;
        return userDto;
    }

    [Authorize]
    [HttpGet("search")]
    public async Task<List<SearchUserDto>> SearchUser([FromQuery] string keyword)
    {
        var userId = _currentUser.GetUserId();
        var users = await _userService.SearchUser(keyword, userId);
        var userDtos = _mapper.Map<List<SearchUserDto>>(users);
        foreach (var userDto in userDtos)
        {
            if (userDto.Followers.Any(f => f.FollowerId == userId))
            {
                userDto.IsFollowing = true;
            }
        }

        userDtos = userDtos.OrderByDescending(x => x.IsFollowing).ToList();
        return userDtos;
    }
}