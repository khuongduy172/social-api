﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Models;
using social_network.src.Services;

namespace social_network.src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly ICurrentUser _currentUser;
        private readonly IStatusService _statusService;
        private readonly IStatusImageService _statusImageService;
        private readonly IStorageService _storageService;

        public StatusController(ICurrentUser currentUser, IStatusService statusService, IStatusImageService statusImageService, IStorageService storageService)
        {
            _currentUser = currentUser;
            _statusService = statusService;
            _statusImageService = statusImageService;
            _storageService = storageService;
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult> CreateStatus([FromForm] CreateStatusRequest request, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            var status = new Status(request.Content, userId);
            await _statusService.Create(status, cancellationToken);

            if (request.Files != null)
            {
                foreach (var item in request.Files)
                {
                    var image = await _storageService.UploadAsync(item);
                    var statusImage = new StatusImage(image.FileName, status.Id, image.Url);
                    await _statusImageService.Create(statusImage, cancellationToken);
                }
            }
            return Ok(status);
        }

        [HttpGet]
        [Route("images/random")]
        public async Task<List<RandomImagesDto>> GetRandomImages()
        {
            return await _statusImageService.GetRandomImagesAsync();
        }

        [HttpGet("{id:guid}")]
        [Authorize]
        public async Task<StatusDto> GetStatusById(Guid id)
        {
            var userId = _currentUser.GetUserId();
            return await _statusService.GetByIdAsync(id, userId);
        }

        [HttpPost("{id:guid}/react")]
        [Authorize]
        public async Task React(Guid id, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _statusService.ReactAsync(id, userId, cancellationToken);
        }

        [HttpGet("{id:guid}/comment")]
        [Authorize]
        public async Task<PaginationResponse<CommentDto>> GetStatusComment(Guid id, [FromQuery] PaginationFilter paginationFilter, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            var comments = await _statusService.GetCommentsAsync(id, userId, paginationFilter, cancellationToken);
            return comments;
        }

        /// <summary>
        /// first get newest and not viewed status then viewed status, last is get randomly
        /// </summary>
        [HttpGet("home")]
        [Authorize]
        public async Task<List<StatusDto>> GetHomeStatuses(CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            return await _statusService.GetHomeStatusesAsync(userId, cancellationToken);
        }

        [HttpPost("{id:guid}/comment")]
        [Authorize]
        public async Task<CommentDto> AddComment(Guid id, [FromBody] CreateCommentRequest createCommentRequest, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            return await _statusService.AddCommentAsync(userId, id, createCommentRequest.Content, cancellationToken);
        }

        [HttpDelete("comment/{cmtId:guid}")]
        [Authorize]
        public async Task<ActionResult<Guid>> DeleteComment(Guid cmtId, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _statusService.DeleteCommentAsync(userId, cmtId, cancellationToken);
            return Ok(cmtId);
        }

        [HttpGet("image/me")]
        [Authorize]
        public async Task<PaginationResponse<StatusImageDto>> GetMyImages([FromQuery] PaginationFilter paginationFilter)
        {
            var userId = _currentUser.GetUserId();
            return await _statusImageService.GetByUserId(userId, paginationFilter);
        }

        [HttpGet("image/{id:guid}")]
        [Authorize]
        public async Task<PaginationResponse<StatusImageDto>> GetUserImages(Guid id, [FromQuery] PaginationFilter paginationFilter)
        {
            return await _statusImageService.GetByUserId(id, paginationFilter);
        }

        [HttpDelete("{id:guid}")]
        [Authorize]
        public async Task<ActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _statusService.DeleteAsync(userId, id, cancellationToken);
            return Ok(id);
        }

        [HttpPost("{id:guid}/view")]
        [Authorize]
        public async Task<ActionResult> ViewStatus(Guid id, CancellationToken cancellationToken)
        {
            var userId = _currentUser.GetUserId();
            await _statusService.View(userId, id, cancellationToken);
            return Ok(id);
        }
    }
}