﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using social_network.src.Common;
using social_network.src.Dtos;
using social_network.src.Hubs;
using social_network.src.Models;
using social_network.src.Services;

namespace social_network.src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FollowController : ControllerBase
    {
        private readonly ICurrentUser _currentUser;
        private readonly IFollowService _followService;
        private readonly IHubContext<CoreHub> _hubContext;
        private readonly IMapper _mapper;

        public FollowController(ICurrentUser currentUser, IFollowService followService, IHubContext<CoreHub> hubContext, IMapper mapper)
        {
            _currentUser = currentUser;
            _followService = followService;
            _hubContext = hubContext;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize]
        public async Task Follow(FollowRequest request, CancellationToken cancellationToken)
        {
            var currentUserId = _currentUser.GetUserId();
            var userId = request.UserId;
            if (userId == currentUserId)
            {
                throw new BadRequestException("can not follow yourself!");
            }

            var follow = await _followService.Get(userId, currentUserId);

            if (follow == null)
            {
                follow = new Follow()
                {
                    UserId = userId,
                    FollowerId = currentUserId,
                };
                await _followService.Follow(follow, cancellationToken);

                var result = await _followService.Get(userId, currentUserId);
                var dto = _mapper.Map<FollowDto>(result);

                await _hubContext.Clients.Groups(request.UserId.ToString()).SendAsync("FollowPushNoti", dto);
            }
        }

        [HttpPost("unfollow")]
        [Authorize]
        public async Task UnFollow(FollowRequest request, CancellationToken cancellationToken)
        {
            var currentUserId = _currentUser.GetUserId();
            var userId = request.UserId;
            if (userId == currentUserId)
            {
                throw new BadRequestException("can not unfollow yourself!");
            }

            var follow = await _followService.Get(userId, currentUserId) ?? throw new NotFoundException("Follow not exist!");

            await _followService.Unfollow(follow, cancellationToken);
        }
    }
}