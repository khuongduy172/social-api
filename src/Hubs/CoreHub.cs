﻿using Mapster;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Dtos;
using social_network.src.Models;

namespace social_network.src.Hubs
{
    public class CoreHub : Hub
    {
        private readonly SocialDbContext _context;
        private readonly ILogger<CoreHub> _logger;

        public CoreHub(SocialDbContext context, ILogger<CoreHub> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task JoinRoom(string roomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
            _logger.Log(LogLevel.Information, "join");
        }

        public async Task Follow(Guid followerId, Guid toUserId, CancellationToken cancellationToken)
        {
            var follower = await _context.Users.FirstAsync(u => u.Id == followerId);
            var followerDto = follower.Adapt<FollowNotiDto>();
            var follow = new Follow(toUserId, followerId);
            _context.Follows.Add(follow);
            await _context.SaveChangesAsync(cancellationToken);

            await Clients.Group(toUserId.ToString()).SendAsync(nameof(Follow), followerDto);
        }

        public async Task Comment(Guid userId, Guid statusId, string content, CancellationToken cancellationToken)
        {
            var comment = new Comment(statusId, userId, content);
            _context.Comments.Add(comment);
            await _context.SaveChangesAsync(cancellationToken);
            await Clients.Group(statusId.ToString()).SendAsync(nameof(Comment));
        }
    }
}