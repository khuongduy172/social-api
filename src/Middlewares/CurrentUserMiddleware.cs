﻿using social_network.src.Services;

namespace social_network.src.Middlewares
{
    public class CurrentUserMiddleware : IMiddleware
    {
        private readonly ICurrentUserInitializer _initializer;

        public CurrentUserMiddleware(ICurrentUserInitializer initializer)
        {
            _initializer = initializer;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            _initializer.SetCurrentUser(context.User);
            await next(context);
        }
    }

    public static class CurrentUserMiddlewareExtensions
    {
        public static IApplicationBuilder UseCurrentUserMiddleware(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CurrentUserMiddleware>();
        }
    }

    public static class CurrentUserExtensions
    {
        public static void AddCurrentUser(this IServiceCollection services)
        {
            services
                .AddScoped<CurrentUserMiddleware>()
                .AddScoped<ICurrentUser, CurrentUser>()
                .AddScoped(sp => (ICurrentUserInitializer)sp.GetRequiredService<ICurrentUser>());
        }
    }
}