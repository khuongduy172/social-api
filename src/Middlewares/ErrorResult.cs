﻿namespace social_network.src.Middlewares
{
    public class ErrorResult
    {
        public List<string>? Messages { get; set; } = new();
        public int StatusCode { get; set; }
    }
}
