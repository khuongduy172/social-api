﻿using Microsoft.EntityFrameworkCore;
using social_network.Data;
using social_network.src.Services;

namespace social_network.src.Seeders;

public class GenerateReelThumbnailSeeder : ICustomSeeder
{
    private readonly SocialDbContext _context;
    private readonly IStorageService _storageService;
    private readonly ILogger<GenerateReelThumbnailSeeder> _logger;

    public GenerateReelThumbnailSeeder(SocialDbContext context, IStorageService storageService, ILogger<GenerateReelThumbnailSeeder> logger)
    {
        _context = context;
        _storageService = storageService;
        _logger = logger;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        var reels = await _context.Reels.Where(x => string.IsNullOrEmpty(x.ThumbnailKey)).ToListAsync(cancellationToken);
        if (reels.Count > 0)
        {
            _logger.LogInformation("Start seeding reel thumbnail");

            try
            {
                foreach (var reel in reels)
                {
                    var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                    var folderName = Path.Combine("Resources", "Images"); // in source code
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    bool folderExists = Directory.Exists(pathToSave);
                    if (!folderExists)
                        Directory.CreateDirectory(pathToSave);
                    var fullPath = Path.Combine(pathToSave, $"video_thumbnail_{reel.Id}.jpg");
                    ffMpeg.GetVideoThumbnail(reel.Url, fullPath);

                    var thumbnail = await _storageService.UploadAsync(fullPath, "image/jpeg");

                    reel.ThumbnailKey = thumbnail.FileName;
                    reel.ThumbnailUrl = thumbnail.Url;
                }

                _context.Reels.UpdateRange(reels);
                await _context.SaveChangesAsync(cancellationToken);
            }
            catch
            {
                _logger.LogError("Error seeding reel thumbnail");
            }

            _logger.LogInformation("Done seeding reel thumbnail");
        }
    }
}