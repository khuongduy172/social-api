﻿using social_network.src.Common.Dapper;
using social_network.src.Enums;
using social_network.src.Models;

namespace social_network.src.Seeders;

public class UpdateNotiTypeBeforeMigration : ICustomSeederBeforeMigration
{
    private readonly ILogger<UpdateNotiTypeBeforeMigration> _logger;
    private readonly IDapperRepository _repository;

    public UpdateNotiTypeBeforeMigration(ILogger<UpdateNotiTypeBeforeMigration> logger, IDapperRepository repository)
    {
        _logger = logger;
        _repository = repository;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        try
        {
            string sql = "select * from Notification where TypeNoti = \"Follow\"";
            var noti = await _repository.QueryAsync<Notification>(sql, cancellationToken: cancellationToken);

            //if (noti.Any())
            //{
            //    _logger.LogInformation("Start seeding NotiType...");

            //    string updateSql = $"update Notification set TypeNoti = \"0\" where TypeNoti = \"{nameof(TypeNotiEnum.Follow)}\";";
            //    await _repository.QueryAsync<string>(updateSql, cancellationToken: cancellationToken);

            //    _logger.LogInformation("Done seeding NotiType.");
            //}
        }
        catch
        {
        }
    }
}