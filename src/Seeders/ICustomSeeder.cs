﻿namespace social_network.src.Seeders;

public interface ICustomSeeder
{
    Task StartAsync(CancellationToken cancellationToken);
}