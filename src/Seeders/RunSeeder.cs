﻿using social_network.Data;

namespace social_network.src.Seeders;

public static class RunSeeder
{
    public static async Task<WebApplication> RunSeederAsync(this WebApplication webApp, CancellationToken cancellationToken = default)
    {
        using var scope = webApp.Services.CreateScope();
        var seeders = scope.ServiceProvider.GetServices<ICustomSeeder>();

        foreach (var seeder in seeders)
        {
            await seeder.StartAsync(cancellationToken);
        }

        return webApp;
    }

    public static async Task<WebApplication> RunSeederBeforeMigrationsAsync(this WebApplication webApp, CancellationToken cancellationToken = default)
    {
        using var scope = webApp.Services.CreateScope();
        var seeders = scope.ServiceProvider.GetServices<ICustomSeederBeforeMigration>();

        foreach (var seeder in seeders)
        {
            await seeder.StartAsync(cancellationToken);
        }

        return webApp;
    }

    public static void RegisterSeeder(this IServiceCollection services)
    {
        #region seed after migration

        services.AddTransient<ICustomSeeder, GenerateReelThumbnailSeeder>();

        #endregion seed after migration

        #region seed before migration

        services.AddTransient<ICustomSeederBeforeMigration, UpdateNotiTypeBeforeMigration>();

        #endregion seed before migration
    }
}