﻿namespace social_network.src.Seeders;

public interface ICustomSeederBeforeMigration
{
    Task StartAsync(CancellationToken cancellationToken);
}