﻿using Microsoft.EntityFrameworkCore;
using social_network.Data;

namespace social_network;

public static class MigrationManager
{
    public static WebApplication MigrateDatabase(this WebApplication webApp)
    {
        using (var scope = webApp.Services.CreateScope())
        {
            var db = scope.ServiceProvider.GetRequiredService<SocialDbContext>();
            db.Database.Migrate();
        }

        return webApp;
    }
}